from Bio import SeqIO
"""
The two first function are taken from the page:
https://open.oregonstate.education/computationalbiology/chapter/python-functions/
So they are the author of it.
"""


def base_composition(seq: str, query_base: str):
    """
    Count the number of times a base appear in a seq
    """
    base_counter = 0
    seq_len = len(seq)
    for index in range(0, seq_len):
        seq_base = seq[index]
        if seq_base == query_base:
            base_counter += 1
    return base_counter


def gc_content(seq: str):
    """
    get GC content from a seq
    """
    g_cont = base_composition(seq, 'G')
    c_cont = base_composition(seq, 'C')
    seq_len = len(seq)
    gc = (g_cont + c_cont)/float(seq_len)
    return gc


def l_and_gc_from_seq(seq_file: str):
    """
Get the length and the gc content from each sequence in the fasta file
    """
    for seq in SeqIO.parse(seq_file, 'fasta'):
        length = len(seq.seq)
        gc_cont = gc_content(seq.seq)
        print(f'{seq.id}\tGC={format(gc_cont, ".4f")}\tlen={length}')
