from pathlib import Path


def eggnog_miner(terms_list_file: str, eggnog_annotation_file: str, output_folder: str,  column: int, logger, sample_name: str):
    """
    This function will mine annotations from eggnog in search of the terms that are in the terms_list_file and create one file per term with the genes that match this annotation so it's easier to mine them by terms.
    """
    with Path(terms_list_file).open('r') as tlf:
        terms_list = tlf.readlines()
    with Path(eggnog_annotation_file).open('r') as eaf:
        eggnog_annotation = eaf.readlines()
    annotation_summary = sample_name
    for tl in terms_list:
        term = tl.split('\t')[0]
        name = tl.strip('\n').split('\t')[1]
        logger.info(f'Processing of the term: {term} - {name}')
        annotation_term = []
        for line in eggnog_annotation:
            if line[0] != '#':
                eggnog_terms = line.strip('\n').split('\t')[column]
                eggnog_terms = eggnog_terms.split(',')
                if term in eggnog_terms:
                    annotation_term.append(line)
        if annotation_term:
            logger.info(
                f'Found {len(annotation_term)} genes for the term. Writting...\n')
            # if at least one gene then save it in a file
            with Path(f'{output_folder}/{term}.txt').open('w') as tf:
                for l in annotation_term:
                    tf.write(l)
        else:
            logger.info('Found no genes for this term\n')
        annotation_summary = annotation_summary + \
            '\t' + str(len(annotation_term))
    with Path(f'{output_folder}/summary_hits.tab').open('w') as sh:
        sh.write(annotation_summary + '\n')
