import re
from pathlib import Path


def name_in_line_finder(name: str, filename: str, logger):
    """
    Find a name (ignore case) in a text file an return which first line and in how many lines the name was found.
    """
    with Path(filename).open('r') as fn:
        text = fn.readlines()
    first_line = 'none'
    count = 0
    n_line = 1
    for line in text:
        if re.search(name, line, re.IGNORECASE):
            count += 1
            if first_line == 'none':
                first_line = n_line
        n_line += 1
    if count > 0:
        match = 'MATCH'
    else:
        match = 'NO_MATCH'
    logger.info(
        f'[{match}]: file = {filename} - first_line = {first_line} - n_matches = {count}')
