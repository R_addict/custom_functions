from pathlib import Path
import tarfile
import gzip


def get_seq(seq_list: list, seq_name: str, header_sign: str, logger):
    seq = []
    found = False
    for item in seq_list:
        if item.startswith(header_sign):
            seq_id = item.split(' ')[0].replace('\n', '')
            if seq_id == f'{header_sign}{seq_name}':
                found = True
                logger.info(f'Sequence with id {seq_name} found!')
        if found:
            if item.startswith(header_sign) and len(seq) > 0:
                break
            seq.append(item)
    if not found:
        logger.info(
            f'[WARNING]: Sequence with id {seq_name} not found in the file')
    return seq


def extract_seqs(seq_file: str, seq_name_file: str, output_file: str, file_type: str, logger):
    """
    A small function to retrieve the sequences by id from a compressed or not fastq or fastq or fasta
    """
    header_dict = {'fastq': '@', 'fasta': '>'}
    if seq_file.endswith('.tar.gz'):
        tar = tarfile.open(seq_file, 'r:gz')
        files = tar.getmembers()
        # if your docs.json is in the 0th position
        seqs_file = tar.extractfile(files[0])
        seqs_file = gzip.open(seqs_file, 'rt')
        content = seqs_file.readlines()
    elif seq_file.endswith('.gz'):
        seqs_file = gzip.open(seq_file, 'rt')
        content = seqs_file.readlines()
    else:
        with Path(seq_file).open('r') as file:
            content = file.readlines()
    with Path(seq_name_file).open('r') as snf:
        seq_names = snf.readlines()
    seq_extracted = []
    for name in seq_names:
        name = name.strip('\n')
        seq_extracted = seq_extracted + \
            get_seq(content, name, header_dict[file_type], logger)
    with Path(output_file).open('w') as of:
        of.writelines(seq_extracted)
