import logging
from pathlib import Path


def set_log(log_file: Path, log_name: str):
    """
    creates and return a log to run in channel + saved in a file.
    """
    logger = logging.getLogger(log_name)
    logger.setLevel(logging.INFO)
    fh = logging.FileHandler(str(log_file))
    fh.setLevel(logging.INFO)
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    # create formatter
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add formatter to ch and fh
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add ch and fh to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    return logger
