from Bio import SeqIO


def rename_fasta(fasta: str, prefix: str):
    """
    Just a function to rename fasta sequences with a number and a prefix so the name is easy to process
    """
    i = 1
    result = []
    for record in SeqIO.parse(fasta, 'fasta'):
        record.id = f'{prefix}_{i}'
        record.description = ''
        i += 1
        result.append(record)
    SeqIO.write(result, fasta, 'fasta')
