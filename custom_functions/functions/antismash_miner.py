from Bio import GenBank
import re
from pathlib import Path


def count_occurences(count_list: list):
    """
    Small function that counts the occurences of all entries of a list to make a list of counts for all the elements of the list
    """
    res = {}
    for i in set(count_list):
        res[i] = count_list.count(i)
    return res


def antismash_miner(gbk_file: str, sample: str, logger):
    with open(gbk_file) as handle:
        cds_annot = []
        protocluster_annot = []
        logger.info(f'Starting the mining of sample: {sample}')
        for record in GenBank.parse(handle):
            for seq_feat in record.features:
                if seq_feat.key == "CDS":
                    for qualifier in seq_feat.qualifiers:
                        if qualifier.key == '/gene_functions=':
                            cds_annot.append(
                                re.sub(' [(]Score.*', '', qualifier.value))
                elif seq_feat.key == 'protocluster':
                    for qualifier_bis in seq_feat.qualifiers:
                        if qualifier_bis.key == '/product=':
                            protocluster_annot.append(qualifier_bis.value)
        logger.info('Mining finished, making the count tables')
        cds_counts = count_occurences(cds_annot)
        protocluster_counts = count_occurences(protocluster_annot)
        logger.info('Writting...')
        with Path(f'{sample}_antismash_protocluster_counts.txt').open('w') as sapc:
            for protocluster in protocluster_counts.keys():
                sapc.write(
                    f'{protocluster}\t{protocluster_counts[protocluster]}\n')

        with Path(f'{sample}_antismash_gene_functions_counts.txt').open('w') as sagfc:
            for gene_function in cds_counts.keys():
                sagfc.write(f'{gene_function}\t{cds_counts[gene_function]}\n')
        logger.info('Finished sucessfully!')
