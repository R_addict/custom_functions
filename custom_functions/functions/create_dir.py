import os
from pathlib import Path


def create_dir(dir_path: Path, logger: Path = 'none'):
    # check if dir exists and if not then create the new director
    if os.path.isdir(dir_path):
        if logger != 'none':
            logger.info(
                f'the directory {dir_path} already exist and will thus not be created'
            )
    else:
        os.makedirs(dir_path)
        if logger != 'none':
            logger.info(f'the directory {dir_path} was created successfully')
