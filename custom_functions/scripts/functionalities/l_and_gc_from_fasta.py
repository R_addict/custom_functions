#!/usr/bin/env python

import argparse
from custom_functions.functions.l_and_gc_from_seq import l_and_gc_from_seq

version = 'l_and_gc_from_fasta_0.1'


def main():
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group('required arguments')
    required.add_argument(
        '-i', '--input_fasta', help='the full path to the input fasta')
    args = parser.parse_args()
    l_and_gc_from_seq(args.input_fasta)
