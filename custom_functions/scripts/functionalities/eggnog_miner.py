import argparse
from custom_functions.functions.set_log import set_log
from custom_functions.functions.create_dir import create_dir
from custom_functions.functions.eggnog_miner import eggnog_miner

version = 'eggnog_miner_0.1'

eggnog_columns = ['query', 'seed_ortholog', 'evalue', 'score', 'eggNOG_OGs', 'max_annot_lvl', 'COG_category', 'Description', 'Preferred_name',
                  'GOs', 'EC', 'KEGG_ko', 'KEGG_Pathway', 'KEGG_Module', 'KEGG_Reaction', 'KEGG_rclass', 'BRITE', 'KEGG_TC CAZy', 'BiGG_Reaction', 'PFAMs']


def main():
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group('required arguments')
    required.add_argument(
        '-a', '--annotation_file', help='path to eggnog mapper annotation file')
    required.add_argument(
        '-o', '--output_folder', help='the full path to the output folder where to store the results')
    required.add_argument(
        '-t', '--terms_file', help='the file that contains the go terms or other terms to mine')
    required.add_argument(
        '-n', '--name', help='the type of terms to mine. eg. GOs for go terms')
    required.add_argument(
        '-s', '--sample_name', help='the name of the sample to process')
    args = parser.parse_args()
    eggnog_miner_logger = set_log('./eggnog_miner.log', version)
    annotation_column = eggnog_columns.index(args.name)
    create_dir(args.output_folder, eggnog_miner_logger)
    eggnog_miner(args.terms_file, args.annotation_file,
                 args.output_folder, annotation_column, eggnog_miner_logger, args.sample_name)
