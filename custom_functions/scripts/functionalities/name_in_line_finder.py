import argparse
from custom_functions.functions.set_log import set_log
from custom_functions.functions.name_in_line_finder import name_in_line_finder

version = 'name_in_line_finder_0.1'


def main():
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group('required arguments')
    required.add_argument(
        '-f', '--file', help='path to the file to mine in text')
    required.add_argument(
        '-n', '--name', help='name to match in the text file')
    args = parser.parse_args()
    nilf_logger = set_log('./name_in_line_finder.log', version)
    name_in_line_finder(args.name, args.file, nilf_logger)
