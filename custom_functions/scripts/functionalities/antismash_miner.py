import argparse
from custom_functions.functions.set_log import set_log
from custom_functions.functions.antismash_miner import antismash_miner

version = 'antismash_miner_0.1'


def main():
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group('required arguments')
    required.add_argument(
        '-f', '--file', help='path to the antismash gbk file')
    required.add_argument(
        '-s', '--sample', help='the sample name')
    args = parser.parse_args()
    antismash_miner_logger = set_log('./antismash_miner.log', version)
    antismash_miner(args.file, args.sample, antismash_miner_logger)
