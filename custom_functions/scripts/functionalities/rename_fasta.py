#!/usr/bin/env python

import argparse
from custom_functions.functions.rename_fasta import rename_fasta

version = 'rename_fasta_0.1'


def main():
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group('required arguments')
    required.add_argument(
        '-i', '--input_file', help='the full path to the input fasta to rename')
    required.add_argument(
        '-p', '--prefix', help='the prefix to add to all the records in the fasta')
    args = parser.parse_args()
    rename_fasta(args.input_file, args.prefix)
