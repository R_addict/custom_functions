#!/usr/bin/env python

import argparse
from custom_functions.functions.extract_seqs import extract_seqs
from custom_functions.functions.set_log import set_log

version = 'extract_seqs_0.1'


def main():
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group('required arguments')
    required.add_argument(
        '-i', '--input_file', help='the full path to the input fastq or fastq (can be compressed)')
    required.add_argument(
        '-o', '--output_file', help='the full path to the output file where to output the seqs')
    required.add_argument(
        '-n', '--names_file', help='the full path to the file containing in one column the names of the samples to simlink the fastq')
    required.add_argument(
        '-t', '--file_type', help='type of file: fasta or fastq')
    args = parser.parse_args()
    extract_seqs_logger = set_log('./extract_seqs.log', version)
    extract_seqs(args.input_file, args.names_file,
                 args.output_file, args.file_type, extract_seqs_logger)
