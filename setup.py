from distutils.core import setup

setup(
    name="custom_functions",
    version="0.1",
    description="custom functions that could be usefull for my own use",
    author="Benoît Bergk Pinto",
    author_email="benoit.bergkpinto@sb-roscoff.fr",
    install_requires=[
        "Bio == 1.5.9"
    ],
    entry_points={
        "console_scripts": [
            "extract_seqs=custom_functions.scripts.functionalities.extract_seqs:main",
            "l_and_gc_from_fasta=custom_functions.scripts.functionalities.l_and_gc_from_fasta:main",
            "eggnog_miner=custom_functions.scripts.functionalities.eggnog_miner:main",
            "rename_fasta=custom_functions.scripts.functionalities.rename_fasta:main",
            "antismash_miner=custom_functions.scripts.functionalities.antismash_miner:main",
            "name_in_line_finder=custom_functions.scripts.functionalities.name_in_line_finder:main",
        ],
    },
    find={},
)
